﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using log4net;

//注意下面的语句一定要加上，指定log4net使用.config文件来读取配置信息
//如果是WinForm（假定程序为MyDemo.exe，则需要一个MyDemo.exe.config文件）
//如果是WebForm，则从web.config中读取相关信息
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Log4NetDemo
{
    /// <summary>
    /// 说明：本程序演示如何利用log4net记录程序日志信息。log4net是一个功能著名的开源日志记录组件。
    /// 利用log4net可以方便地将日志信息记录到文件、控制台、Windows事件日志和数据库中（包括MS SQL Server, Access, Oracle9i,Oracle8i,DB2,SQLite）。
    /// 下面的例子展示了如何利用log4net记录日志
    /// 作者：周公
    /// 时间：2008-3-26
    /// 首发地址：http://blog.csdn.net/zhoufoxcn/archive/2008/03/26/2220533.aspx
    /// </summary>
    public class MainClass
    {
        public static void Main(string[] args)
        {
            //Application.Run(new MainForm());
            //创建日志记录组件实例
            ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            //记录错误日志
            log.Error("error",new Exception("发生了一个异常"));
            //记录严重错误
            log.Fatal("fatal",new Exception("发生了一个致命错误"));
            //记录一般信息
            log.Info("info");
            //记录调试信息
            log.Debug("debug");
            //记录警告信息
            log.Warn("warn");
            Console.WriteLine("日志记录完毕。");
            Console.Read();
        }
    }
}