﻿using System;
using System.Data ;
using System.Collections.Generic;
using System.Web;
using MySql.Data.MySqlClient;
using log4net ;
using System.Text;
using System.Reflection;
/// <summary>
/// Summary description for Test
/// </summary>
public class Test
{
    public Test()
    {
        //
        // TODO: Add constructor logic here
        //
        //MySqlHelper.connectionStringManager() ;
    }
    /**
     * 登录验证
     */
    public string Login(string mobile,string password){
        ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        //记录错误日志
        log.Error("error",new Exception("发生了一个异常"));
        //记录严重错误
        log.Fatal("fatal",new Exception("发生了一个致命错误"));
        //记录一般信息
        log.Info("info");
        //记录调试信息
        log.Debug("debug");
        //记录警告信息
        log.Warn("warn");

        MySqlCommand sqlcom = new MySqlCommand();
        sqlcom.CommandText = "select * from users where mobile =@mobile";
        MySqlParameter commandParameters = new MySqlParameter("@mobile", mobile);
        MySqlDataReader reader = MySqlHelper.ExecuteReader(MySqlHelper.ConnectionStringManager, CommandType.Text, sqlcom.CommandText, commandParameters);
        if (reader.Read() == true){
            string pwd = reader["password"].ToString();
            Console.Write(pwd) ;
            if (pwd.Equals(password)){
                return "1";
            }else{
                return "0";
            }                
        }
        else{
            return "-1";
        }
    }
    //列表
    public string list(){
        return "" ;
    }
}
